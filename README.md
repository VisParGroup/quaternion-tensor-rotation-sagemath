# Quaternion rotations in SageMath #

This repository provides functions and examples for using quaternions to rotate vectors and tensors. 
Further functions for conversion between Cardan angles and quaternions are provided.

This is supplementary to the publication ...
